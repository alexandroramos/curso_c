/* ----------------------------------------------------------------------------
 * Exercício 1: Utilização da função printf()
 * e especificadores de formato.
 * 
 * Autor: Alexandro Ramos
 ---------------------------------------------------------------------------- */

#include <stdio.h>

int main()
{
  // Cabeçalho
  printf("\n===========================================================\n");
  printf("Programação em C - Básico\n");
  printf("Aula 2");
  printf("\n===========================================================\n\n");
  
  // Dados do Aluno
  printf("Matrícula: %d\n", 2017204235);
  printf("Aluno: %s\n", "Alexandro Ramos");
  
  // Disciplinas e Notas
  printf("\n-----------------------------------------------------------\n");
  printf("Disciplina\t\tNota\n");
  printf("Matemática Discreta\t%.2f\n", 8.5);
  printf("Programação I\t\t%.2f", 9.5);
  printf("\n-----------------------------------------------------------\n\n");

  // Rodapé
  printf("\"Programa executado com Sucesso :)\"\n\n");

  return 0;
}
