/* ----------------------------------------------------------------------------
 * Exemplo Aula 4:
 * - Declaração de variáveis e constantes;
 * - Operadores aritméticos;
 *
 * Este programa calcula a média aritmética de 3 números.
 * 
 * Autor: Alexandro Ramos
 ---------------------------------------------------------------------------- */

#include <stdio.h>

#define MAX 3 //Declaração da constante

int main()
{
  // Declaração de variáveis e inicialização
  float num1 = 10.0;
  float num2 = 8.5;
  float num3 = 7.0;
  float media;
  
  // Cálculo da média
  media = (num1 + num2 + num3)/MAX;
  
  // Impressão na saída padrão
  printf("A média é: %.2f\n", media);

  return 0;
}
